//
//  IEANMultitranParser.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IEANMultitranParserDelegate;

@interface IEANMultitranParser : NSObject
@property (nonatomic, assign) id<IEANMultitranParserDelegate> delegate;

- (instancetype)initWithData:(NSData *)data;

- (void)parse;

@end

@protocol IEANMultitranParserDelegate <NSObject>

- (void)parser:(IEANMultitranParser *)parser didEndParsingDocumentWithResults:(NSArray *)results;

- (void)parserDidNotFoundAnyTranslations:(IEANMultitranParser *)parser;

@end