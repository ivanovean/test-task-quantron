//
//  IEANMultitranParser.m
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import "IEANMultitranParser.h"
#import "HTMLNode.h"
#import "HTMLParser.h"
#import "IEANTranslationWordFormObject.h"
#import "IEANTranslationScopeObject.h"

static NSString *kIEANMultitranParserNotFoundMarker  = @"Вы знаете перевод";
static NSString *kIEANMultitranParserPartFoundMarker = @"найдены отдельные";
static NSString *kIEANMultitranParserTableMarker     = @"createAutoComplete();";
static NSString *kIEANMultitranParserTableBeginTag   = @"<table";
static NSString *kIEANMultitranParserTableEndTag     = @"</table>";
static NSString *kIEANMultitranParserHTMLTagTr       = @"tr";
static NSString *kIEANMultitranParserHTMLTagTd       = @"td";
static NSString *kIEANMultitranParserHTMLTagTrFull   = @"<tr>";
static NSString *kIEANMultitranParserHTMLTagsRegex   = @"<[^>]+>";
static NSString *kIEANMultitranParserHTMLTrash       = @"в начало";


@implementation IEANMultitranParser {
    NSString * _inputString;
    NSString * _translationTableString;
    NSMutableArray *_resultsTranslationWordFormObjects;
}

#pragma mark - Init

- (instancetype)initWithData:(NSData *)data {
    if (self = [super init]) {
        _inputString = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
    }
    return self;
}

#pragma mark - Parsing

- (void)parse {
    if (([_inputString rangeOfString:kIEANMultitranParserNotFoundMarker options:NSCaseInsensitiveSearch].location == NSNotFound)||([_inputString rangeOfString:kIEANMultitranParserPartFoundMarker options:NSCaseInsensitiveSearch].location != NSNotFound)) {
        
        _resultsTranslationWordFormObjects = [NSMutableArray new];
        
        //Search table of translations
        NSRange rangeCutMarker = [_inputString rangeOfString:kIEANMultitranParserTableMarker options:NSCaseInsensitiveSearch];
        NSString *cutString = [_inputString substringFromIndex:rangeCutMarker.location + rangeCutMarker.length + 1];
        
        NSRange beginTableRange = [cutString rangeOfString:kIEANMultitranParserTableBeginTag options:NSCaseInsensitiveSearch];
        NSRange endTableRange = [cutString rangeOfString:kIEANMultitranParserTableEndTag options:NSCaseInsensitiveSearch];
        _translationTableString = [cutString substringWithRange:NSMakeRange(beginTableRange.location, endTableRange.location + endTableRange.length - beginTableRange.location )];
        
        //parsing with HTMLparser
        NSError *error = nil;
        HTMLParser *parser = [[HTMLParser alloc] initWithString:_translationTableString error:&error];
        HTMLNode *bodyNode = [parser body];
        
        IEANTranslationWordFormObject *currentWordFormObject = nil;
        
        //all table rows
        NSArray *tableRows = [bodyNode findChildTags:kIEANMultitranParserHTMLTagTr];
        
        for (HTMLNode *currentRowNode in tableRows) {
            // cells in this row
            NSArray *tableCellsInCurrentRow = [currentRowNode findChildTags:kIEANMultitranParserHTMLTagTd];
            
            if (tableCellsInCurrentRow.count == 1) {
                HTMLNode *node = [tableCellsInCurrentRow firstObject];
                NSString *rawNodeContent = [[node rawContents] stringByReplacingOccurrencesOfString:kIEANMultitranParserHTMLTrash withString:@""];
                if (currentWordFormObject) {
                    [_resultsTranslationWordFormObjects addObject:currentWordFormObject];
                }
                currentWordFormObject = [[IEANTranslationWordFormObject alloc] initWithWordForm:[self stringByRemovingAllTagsFromString:rawNodeContent]];
            }
            if (tableCellsInCurrentRow.count > 1) {
                NSString *rawNodeTd0Content = [[tableCellsInCurrentRow objectAtIndex:0] rawContents];
                NSString *rawNodeTd1Content = [[tableCellsInCurrentRow objectAtIndex:1] rawContents];
                if (currentWordFormObject) {
                    IEANTranslationScopeObject *currentScopeObject = [[IEANTranslationScopeObject alloc] initWithScope:[self stringByRemovingAllTagsFromString:rawNodeTd0Content] andTranslation:[self stringByRemovingAllTagsFromString:rawNodeTd1Content]];
                    [currentWordFormObject addTranslationScopeObject:currentScopeObject];
                }
            }
        }
        if (currentWordFormObject) {
            [_resultsTranslationWordFormObjects addObject:currentWordFormObject];
        }
        
        if (_resultsTranslationWordFormObjects.count >0) {
            [self.delegate parser:self didEndParsingDocumentWithResults:[_resultsTranslationWordFormObjects copy]];
        } else {
            // TODO NSLog error!!!
            [self.delegate parserDidNotFoundAnyTranslations:self];
        }
        
    } else {
        [self.delegate parserDidNotFoundAnyTranslations:self];
    }
    
}

- (NSString *)stringByRemovingAllTagsFromString:(NSString *)tString {
    NSString *newString = [tString copy];
    
    // delete <tr>. Hack because html broken.
    NSRange rangeTr = [newString rangeOfString:kIEANMultitranParserHTMLTagTrFull options:NSCaseInsensitiveSearch];
    if (rangeTr.location != NSNotFound) {
        newString = [newString substringToIndex:rangeTr.location];
    }
    // delete all html tags
    NSRange currentRange;
    while ((currentRange = [newString rangeOfString:kIEANMultitranParserHTMLTagsRegex options:NSRegularExpressionSearch]).location!=NSNotFound)
        newString = [newString stringByReplacingCharactersInRange:currentRange withString:@""];
    return [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
