//
//  IEANDownloadHandler.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEANTranslationObject.h"
#import "IEANMultitranParser.h"

@protocol IEANTranslateHandlerDelegate;

@interface IEANTranslateHandler : NSObject <IEANMultitranParserDelegate>

- (instancetype)initWithDelegate:(id<IEANTranslateHandlerDelegate>)delegate;

- (void)translateWord:(NSString *)wordToTranslate language:(IEANTranslationLangEnum)langEnum;

@end

@protocol IEANTranslateHandlerDelegate <NSObject>

- (void)translateHandlerDidEndTranslate: (IEANTranslateHandler *)translateHandler;

- (void)translateHandlerDidFailTranslate: (IEANTranslateHandler *)translateHandler;

- (void)translateHandlerDidFailDownload: (IEANTranslateHandler *)translateHandler;

@end