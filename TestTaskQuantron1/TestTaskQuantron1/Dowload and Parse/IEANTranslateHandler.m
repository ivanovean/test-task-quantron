//
//  IEANDownloadHandler.m
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import "IEANTranslateHandler.h"
#import "IEANTranslationObject.h"
#import "IEANDataSingleton.h"

static NSString * kIEANTranslateHandlerURLPart1 = @"http://www.multitran.ru/c/m.exe?l1=";
static NSString * kIEANTranslateHandlerURLPart2 = @"&l2=2&s=";

static NSString * kIEANTranslateHandler_KEY_FAIL_URL = @"ERROR_URL";
static NSString * kIEANTranslateHandler_KEY_STATUS_CODE= @"WITH_STATUS_CODE";


@implementation IEANTranslateHandler {
    __weak id<IEANTranslateHandlerDelegate> _delegate;
    IEANTranslationObject * _translationObject;
}

#pragma mark - Init

- (instancetype)initWithDelegate:(id<IEANTranslateHandlerDelegate>)delegate {
    if (self = [super init]) {
        _delegate = delegate;
        _translationObject = nil;
    }
    return self;
}

#pragma mark - Translation

- (void)translateWord:(NSString *)wordToTranslate language:(IEANTranslationLangEnum)langEnum {
    NSString *stringURL = [NSString stringWithFormat:@"%@%lu%@%@",kIEANTranslateHandlerURLPart1, (long)langEnum, kIEANTranslateHandlerURLPart2, [wordToTranslate stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *url = [NSURL URLWithString:stringURL];
    NSURLSessionDataTask *translateTask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (httpResp.statusCode == 200) {
                
                _translationObject = [[IEANTranslationObject alloc] initWithWord:wordToTranslate andLanguage:langEnum];
                IEANMultitranParser *parser = [[IEANMultitranParser alloc] initWithData:data];
                parser.delegate = self;
                [parser parse];
                
            } else {
                NSLog(@"%@ %@ %@ %ld", NSLocalizedString(kIEANTranslateHandler_KEY_FAIL_URL, nil),url, NSLocalizedString(kIEANTranslateHandler_KEY_STATUS_CODE, nil), (long)httpResp.statusCode);
                [_delegate translateHandlerDidFailDownload:self];
            }
        
        } else {
            NSLog(@"%@", [error localizedDescription]);
            [_delegate translateHandlerDidFailDownload:self];
        }
    }];
    
    [translateTask resume];
    
}

#pragma mark - IEANMultitranParserDelegate Protocol

- (void)parser:(IEANMultitranParser *)parser didEndParsingDocumentWithResults:(NSArray *)results {
    [_translationObject addTranslationWordFormObjectsFromArray:results];
    [[IEANDataSingleton sharedInstance] addTranslationObject:_translationObject];
    [_delegate translateHandlerDidEndTranslate:self];
}

- (void)parserDidNotFoundAnyTranslations:(IEANMultitranParser *)parser {
    [_delegate translateHandlerDidFailTranslate:self];
}

@end
