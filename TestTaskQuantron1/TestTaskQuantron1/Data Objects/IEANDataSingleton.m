//
//  IEANDataSingleton.m
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import "IEANDataSingleton.h"

static NSString *kIEANDataSingletonCodeTranslationsArray = @"translationsArray";

static NSUInteger kIEANDataSingletonMaxArrayCount = 5;

@implementation IEANDataSingleton {
    NSMutableArray *_translationObjectsArray;
}

#pragma mark - Init

+(id)sharedInstance {
    static dispatch_once_t pred;
    static IEANDataSingleton *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[IEANDataSingleton alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        NSArray *storedArray = [self loadTranslations];
        if (storedArray) {
            _translationObjectsArray = [NSMutableArray arrayWithArray:storedArray];
        } else {
            _translationObjectsArray = nil;
        }
        
    }
    return  self;
}

#pragma mark - Tranlations Array management

- (NSArray *)translationObjects {
    return [_translationObjectsArray copy];
}

- (IEANTranslationObject *)translationObjectAtIndex:(NSUInteger)index {
    return [_translationObjectsArray objectAtIndex:index];
}

- (void)addTranslationObject:(IEANTranslationObject *)aTranslationObject {
    if (!_translationObjectsArray) {
        _translationObjectsArray = [NSMutableArray new];
    }
    if (_translationObjectsArray.count >= kIEANDataSingletonMaxArrayCount) {
        [_translationObjectsArray removeObjectAtIndex:0];
    }
    [_translationObjectsArray addObject:aTranslationObject];
    [self saveTranslations];
}

- (BOOL)isLastObjectWord:(NSString *)aWord andLanguage:(IEANTranslationLangEnum)aLangEnum {
    IEANTranslationObject *lastObject = [_translationObjectsArray lastObject];
    if (lastObject) {
        if([lastObject.wordForTranslation caseInsensitiveCompare:aWord] == NSOrderedSame) {
            if (lastObject.langEnum == aLangEnum) {
                return YES;
            }
        }
    }
    return NO;
}

#pragma mark - Save/Load data

- (void)saveTranslations {
    if (_translationObjectsArray.count > 0) {
        NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:_translationObjectsArray];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:archivedData forKey:kIEANDataSingletonCodeTranslationsArray];
        [defaults synchronize];
    }
}

- (NSArray *)loadTranslations {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *archiviedData = [defaults objectForKey:kIEANDataSingletonCodeTranslationsArray];
    return [NSKeyedUnarchiver unarchiveObjectWithData:archiviedData];
}

@end
