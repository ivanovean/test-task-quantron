//
//  IEANTranslationObject.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, IEANTranslationLangEnum) {
    IEANLangEng = 1,
    IEANLangGer = 3,
    IEANLangFre = 4,
};

@interface IEANTranslationObject : NSObject <NSCoding>
@property (nonatomic, readonly) NSString * wordForTranslation;
@property (nonatomic, readonly) IEANTranslationLangEnum langEnum;

- (instancetype)initWithWord:(NSString *)aWord andLanguage:(IEANTranslationLangEnum)aLangEnum;

- (void)addTranslationWordFormObjectsFromArray:(NSArray *)wordFormArray;

- (NSArray *)translationsArray;

@end
