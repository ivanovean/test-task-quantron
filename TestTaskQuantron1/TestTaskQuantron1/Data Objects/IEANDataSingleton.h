//
//  IEANDataSingleton.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEANTranslationObject.h"

@interface IEANDataSingleton : NSObject

+ (id)sharedInstance;

- (NSArray *)translationObjects;
- (IEANTranslationObject *)translationObjectAtIndex:(NSUInteger)index;
- (void)addTranslationObject:(IEANTranslationObject *)aTranslationObject;

- (BOOL)isLastObjectWord:(NSString *)aWord andLanguage:(IEANTranslationLangEnum)aLangEnum;

@end
