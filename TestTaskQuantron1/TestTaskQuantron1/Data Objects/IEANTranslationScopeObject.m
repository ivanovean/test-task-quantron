//
//  IEANTranslationScopeObject.m
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import "IEANTranslationScopeObject.h"

static NSString *kIEANTranslationScopeObjectCodeScope       = @"scope";
static NSString *kIEANTranslationScopeObjectCodeTranslation = @"translation";


@implementation IEANTranslationScopeObject
@synthesize scope = _scope;
@synthesize translation = _translation;

#pragma mark - Init

- (instancetype)initWithScope:(NSString *)aScope andTranslation:(NSString *)aTranslation {
    if (self = [super init]) {
        _scope = aScope;
        _translation = aTranslation;
    }
    return self;
}

#pragma mark - NSCoding Protocol

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _scope = [aDecoder decodeObjectForKey:kIEANTranslationScopeObjectCodeScope];
        _translation = [aDecoder decodeObjectForKey:kIEANTranslationScopeObjectCodeTranslation];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.scope forKey:kIEANTranslationScopeObjectCodeScope];
    [aCoder encodeObject:self.translation forKey:kIEANTranslationScopeObjectCodeTranslation];
    
}

@end
