//
//  IEANTranslationScopeObject.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IEANTranslationScopeObject : NSObject <NSCoding>
@property (nonatomic, readonly) NSString * scope;
@property (nonatomic, readonly) NSString * translation;

- (instancetype)initWithScope:(NSString *)aScope andTranslation:(NSString *)aTranslation;

@end
