//
//  IEANTranslationWordFormObject.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEANTranslationScopeObject.h"

@interface IEANTranslationWordFormObject : NSObject <NSCoding>
@property (nonatomic, readonly) NSString * wordForm;

- (instancetype)initWithWordForm:(NSString *)aWordForm;

- (void)addTranslationScopeObject:(IEANTranslationScopeObject *)scopeObject;

- (NSArray *)translationScopeObjectsArray;

@end
