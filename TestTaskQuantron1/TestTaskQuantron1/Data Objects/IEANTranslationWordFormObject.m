//
//  IEANTranslationWordFormObject.m
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import "IEANTranslationWordFormObject.h"

static NSString * kIEANTranslationWordFormObjectCodeWordForm = @"wordForm";
static NSString * kIEANTranslationWordFormObjectCodeScopeTranslations = @"scopeTranslations";

@implementation IEANTranslationWordFormObject {
    NSMutableArray *_scopeTranslationsArray;

}
@synthesize wordForm = _wordForm;

#pragma mark - Init

- (instancetype)initWithWordForm:(NSString *)aWordForm {
    if (self = [super init]) {
        _wordForm = aWordForm;
        _scopeTranslationsArray = nil;
    }
    return self;
}

#pragma mark - ScobeObjects Array Management

- (void)addTranslationScopeObject:(IEANTranslationScopeObject *)scopeObject {
    if (!_scopeTranslationsArray) {
        _scopeTranslationsArray = [NSMutableArray new];
    }
    [_scopeTranslationsArray addObject:scopeObject];
}

- (NSArray *)translationScopeObjectsArray {
    return [_scopeTranslationsArray copy];
}

#pragma mark - NSCoding Protocol

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _wordForm = [aDecoder decodeObjectForKey:kIEANTranslationWordFormObjectCodeWordForm];
        _scopeTranslationsArray = [aDecoder decodeObjectForKey:kIEANTranslationWordFormObjectCodeScopeTranslations];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.wordForm forKey:kIEANTranslationWordFormObjectCodeWordForm];
    [aCoder encodeObject:_scopeTranslationsArray forKey:kIEANTranslationWordFormObjectCodeScopeTranslations];
    
}

@end
