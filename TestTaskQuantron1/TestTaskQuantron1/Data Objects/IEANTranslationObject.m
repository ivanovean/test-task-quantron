//
//  IEANTranslationObject.m
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 05.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import "IEANTranslationObject.h"

static NSString * kIEANTranslationObjectCodeWordForTranslation = @"wordForTranslation";
static NSString * kIEANTranslationObjectCodeLangEnum = @"langEnum";
static NSString * kIEANTranslationObjectCodeTranslationWordForms = @"translationWordForms";

@implementation IEANTranslationObject {
    NSMutableArray *_translationWordFormArray;
}
@synthesize wordForTranslation = _wordForTranslation;
@synthesize langEnum = _langEnum;

#pragma mark - Init

- (instancetype)initWithWord:(NSString *)aWord andLanguage:(IEANTranslationLangEnum)aLangEnum {
    if (self = [super init]) {
        _wordForTranslation = aWord;
        _langEnum = aLangEnum;
    }
    return self;
}

#pragma mark - WordForms Array Management

- (void)addTranslationWordFormObjectsFromArray:(NSArray *)wordFormArray {
    if (!_translationWordFormArray) {
        _translationWordFormArray = [NSMutableArray new];
    }
    [_translationWordFormArray addObjectsFromArray:wordFormArray];
}

- (NSArray *)translationsArray {
    return [_translationWordFormArray copy];
}

#pragma mark - NSCoding Protocol

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _wordForTranslation = [aDecoder decodeObjectForKey:kIEANTranslationObjectCodeWordForTranslation];
        _langEnum = [aDecoder decodeIntegerForKey:kIEANTranslationObjectCodeLangEnum];
        _translationWordFormArray = [aDecoder decodeObjectForKey:kIEANTranslationObjectCodeTranslationWordForms];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.wordForTranslation forKey:kIEANTranslationObjectCodeWordForTranslation];
    [aCoder encodeInteger:self.langEnum forKey:kIEANTranslationObjectCodeLangEnum];
    [aCoder encodeObject:_translationWordFormArray forKey:kIEANTranslationObjectCodeTranslationWordForms];
    
}

@end
