//
//  IEANHistoryTableViewController.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 06.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IEANHistoryTableViewControllerDelegate;

@interface IEANHistoryTableViewController : UITableViewController
@property (nonatomic, assign) id<IEANHistoryTableViewControllerDelegate> delegate;

@end

@protocol IEANHistoryTableViewControllerDelegate <NSObject>

- (void)historyController:(IEANHistoryTableViewController*)controller didSelectObjectAtIndex:(NSInteger)index;

@end