//
//  ViewController.m
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 04.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import "IEANMainViewController.h"
#import "IEANTranslationObject.h"
#import "IEANTranslationWordFormObject.h"
#import "IEANTranslationScopeObject.h"
#import "IEANDataSingleton.h"

static NSString * kIEANMainViewControllerHistorySegueIdentifier  = @"IEANHistorySegueIdentifier";

static NSString * kIEANMainViewController_KEY_ERROR_TITLE_TEXT   = @"ERROR_TITLE_TEXT";
static NSString * kIEANMainViewController_KEY_ERROR_CANCEL_TEXT  = @"ERROR_CANCEL_TEXT";
static NSString * kIEANMainViewController_KEY_ERROR_NETWORK_TEXT = @"ERROR_NETWORK_TEXT";
static NSString * kIEANMainViewController_KEY_ERROR_EMPTY_TEXT   = @"ERROR_EMPTY_TEXT";
static NSString * kIEANMainViewController_KEY_NOTRANSLATED_TEXT  = @"NOTRANSLATED_TEXT";

@interface IEANMainViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *languageSegmentedControl;
@property (weak, nonatomic) IBOutlet UITextView *translationsTextView;
@property (weak, nonatomic) IBOutlet UIView *indicatorContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * indicator;
@property (weak, nonatomic) IBOutlet UIBarButtonItem * historyButton;
@property (weak, nonatomic) IBOutlet UIButton * translateButton;

@end

@implementation IEANMainViewController {
    IEANTranslateHandler *_translateHandler;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.indicatorContainer.hidden = YES;
    [self setControlsEnabled:YES];
    _translateHandler = [[IEANTranslateHandler alloc] initWithDelegate:self];
    self.textField.delegate = self;
    [self.languageSegmentedControl addTarget:self action:@selector(langSegmentedChanged:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - Translation

- (IBAction)translateButtonTapped:(id)sender {
    [self translateCurrentWord];
}

- (void)translateCurrentWord {
    [self.textField endEditing:YES];
    self.textField.text = [self.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([self.textField.text length] != 0) {
        NSString *currentWord = self.textField.text;
        IEANTranslationLangEnum currentLang = [self currentLangFromSegmentControl];
        if ([[IEANDataSingleton sharedInstance] isLastObjectWord:currentWord andLanguage:currentLang]) {
            [self configureViewForTranslationObject:[[[IEANDataSingleton sharedInstance] translationObjects] lastObject]];
        } else {
            [self startLoadingIndicator];
            [_translateHandler translateWord:currentWord language:currentLang];
        }
    } else {
        UIAlertView *emptyFieldAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(kIEANMainViewController_KEY_ERROR_TITLE_TEXT, nil) message:NSLocalizedString(kIEANMainViewController_KEY_ERROR_EMPTY_TEXT, nil) delegate:nil cancelButtonTitle:NSLocalizedString(kIEANMainViewController_KEY_ERROR_CANCEL_TEXT, nil) otherButtonTitles:nil];
        [emptyFieldAlertView show];
    }
}

#pragma mark - Configure View

- (void)configureViewForTranslationObject:(IEANTranslationObject *)translationObject {
    self.textField.text = translationObject.wordForTranslation;
    [self setSelectSegmentForLang:translationObject.langEnum];
    [self configureTextViewForTranslationsArray:[translationObject translationsArray]];
}

- (void)configureTextViewForTranslationsArray:(NSArray *)wordFormsArray {
    NSMutableString *stringForTextView = [NSMutableString stringWithString:@""];
    for (IEANTranslationWordFormObject *currentWordForm in wordFormsArray) {
        [stringForTextView appendFormat:@"%@\r\r", currentWordForm.wordForm];
        for (int i = 0; i<currentWordForm.translationScopeObjectsArray.count; i++) {
            IEANTranslationScopeObject *currentScopeObject = [currentWordForm.translationScopeObjectsArray objectAtIndex:i];
            [stringForTextView appendFormat:@"%d. %@  %@\r\r",i+1, currentScopeObject.scope, currentScopeObject.translation];
        }
        [stringForTextView appendString:@"\r\r"];
    }
    self.translationsTextView.text = stringForTextView;
}

- (void)configureTextViewForNotFoundTranslations {
    self.translationsTextView.text = NSLocalizedString(kIEANMainViewController_KEY_NOTRANSLATED_TEXT, nil);
}

- (void)clearTextView {
    self.translationsTextView.text = @"";
}

- (void)setControlsEnabled:(BOOL)flag {
    self.historyButton.enabled = flag;
    self.translateButton.enabled = flag;
    self.textField.enabled = flag;
    self.languageSegmentedControl.enabled = flag;
}

#pragma mark - SegmentedControl

- (IEANTranslationLangEnum)currentLangFromSegmentControl {
    switch (self.languageSegmentedControl.selectedSegmentIndex) {
        case 0:
            return IEANLangEng;
        case 1:
            return IEANLangGer;
        case 2:
            return IEANLangFre;
        default:
            return IEANLangEng;
    }
}

- (void)setSelectSegmentForLang:(IEANTranslationLangEnum)lang {
    switch (lang) {
        case IEANLangEng:
            [self.languageSegmentedControl setSelectedSegmentIndex:0];
            break;
        case IEANLangGer:
            [self.languageSegmentedControl setSelectedSegmentIndex:1];
            break;
        case IEANLangFre:
            [self.languageSegmentedControl setSelectedSegmentIndex:2];
            break;
        default:
            [self.languageSegmentedControl setSelectedSegmentIndex:0];
            break;
    }
}

- (void)langSegmentedChanged:(id)sender {
    if ([sender isEqual:self.languageSegmentedControl]) {
        [self clearTextView];
        if ([self.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length != 0) {
                [self translateCurrentWord];
        }
    }
}

#pragma mark - Indicator

- (void)startLoadingIndicator {
    [self setControlsEnabled:NO];
    self.indicatorContainer.hidden = NO;
    [self.indicator startAnimating];
}


- (void)stopLoadingIndicator {
    [self setControlsEnabled:YES];
    self.indicatorContainer.hidden = YES;
    [self.indicator stopAnimating];
}

#pragma mark - UITextFieldDelegate Protocol

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self clearTextView];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self clearTextView];
    [self translateCurrentWord];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self clearTextView];
    return YES;
}

#pragma mark - IEANTranslateHandlerDelegate Protocol

- (void)translateHandlerDidEndTranslate: (IEANTranslateHandler *)translateHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopLoadingIndicator];
        [self configureViewForTranslationObject:[[[IEANDataSingleton sharedInstance] translationObjects] lastObject]];
        
    });
}

- (void)translateHandlerDidFailTranslate: (IEANTranslateHandler *)translateHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopLoadingIndicator];
        [self configureTextViewForNotFoundTranslations];
    });
}

- (void)translateHandlerDidFailDownload: (IEANTranslateHandler *)translateHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopLoadingIndicator];
        
        UIAlertView *failDownloadAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(kIEANMainViewController_KEY_ERROR_TITLE_TEXT, nil) message:NSLocalizedString(kIEANMainViewController_KEY_ERROR_NETWORK_TEXT, nil) delegate:nil cancelButtonTitle:NSLocalizedString(kIEANMainViewController_KEY_ERROR_CANCEL_TEXT, nil) otherButtonTitles:nil];
        [failDownloadAlertView show];
        
    });
}

#pragma mark - IEANHistoryTableViewControllerDelegate Protocol

- (void)historyController:(IEANHistoryTableViewController*)controller didSelectObjectAtIndex:(NSInteger)index {
    if ([[IEANDataSingleton sharedInstance] translationObjects].count > index) {
        [self configureViewForTranslationObject:[[[IEANDataSingleton sharedInstance] translationObjects] objectAtIndex:index]];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kIEANMainViewControllerHistorySegueIdentifier]) {
        ((IEANHistoryTableViewController *)[[[segue destinationViewController] viewControllers] objectAtIndex:0]).delegate = self;
    }
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
