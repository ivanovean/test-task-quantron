//
//  ViewController.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 04.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IEANTranslateHandler.h"
#import "IEANHistoryTableViewController.h"

@interface IEANMainViewController : UIViewController <IEANTranslateHandlerDelegate, IEANHistoryTableViewControllerDelegate, UITextFieldDelegate>


@end

