//
//  AppDelegate.h
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 04.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IEANAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

