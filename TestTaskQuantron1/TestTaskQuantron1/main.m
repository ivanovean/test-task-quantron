//
//  main.m
//  TestTaskQuantron1
//
//  Created by Ivanov Evgeniy on 04.12.14.
//  Copyright (c) 2014 Ivanov Evgeniy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IEANAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IEANAppDelegate class]));
    }
}
